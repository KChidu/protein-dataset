f=open("protienData.txt","r") #opened data for reading purpose
o=open("mapperoutput.txt","w") #opened data to enter key value pairs

for line in f:
    input = line.strip().split(',')
    if (len(input) == 14):
        structureId,classification,experimentalTechnique,macromoleculeType,residueCount,resolution,structureMolecularWeight,crystallizationMethod,crystallizationTempK,densityMatthews,densityPercentSol,pdbxDetails,phValue,publicationYear = input
        # writing the values of classification and residueCount into the file only if the classification 
        # value is not a null value
        if (classification):
            o.write("{0}\t{1}\n".format(classification, residueCount))
            print classification + '\t' + residueCount # to print the values in console

f.close()
o.close()