f=open("Data.txt","r") #opens the file-read only
o=open("mapper_output.txt","w") #opens the file-write only
for line in f:
    datalist=line.strip().split(",") #splitting the data with ,
    if (len(datalist) == 14):
         # saving the data t0 file only if the data is not null
     structureId, classification, experimentalTechnique, macromoleculeType, residueCount, resolution, structureMolecularWeight, crystallizationMethod, crystallizationTempK, densityMatthews, densityPerSol, pdbxDetails, phValue, publicationYear = datalist
     o.write(macromoleculeType + '\t' + structureMolecularWeight + '\n')
     print '%s\t%s\n'% (macromoleculeType, structureMolecularWeight)  #printing to the console
f.close() #closing the data file
o.close() #closing the mapper output file