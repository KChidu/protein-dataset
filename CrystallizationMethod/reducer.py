s = open("sorted.txt","r")   # open file, read-only
r = open("reducedata.txt", "w")  # open file, write 

thisKey = ""
thisValue = 0.0

for line in s:
    data = line.strip().split('\t')
    crystallizationMethod, densityPercentSol = data

    if crystallizationMethod != thisKey:
     if thisKey:
      # output the last key value pair result
      r.write(thisKey + '\t' + str(maxi)+'\n')

     # start over when changing keys
     thisKey = crystallizationMethod
     thisValue = 0.0
     maxi=0
 # apply the max function
    if (densityPercentSol>maxi):
        maxi=densityPercentSol
#print maxi
 # output the final entry when done (outside for loop)
r.write(thisKey + '\t' + str(maxi)+'\n')
