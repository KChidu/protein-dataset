i = open("data.txt", "r")
next(i)
o = open("mapperdata.txt", "w")


 

for line in i:
    
    data = line.strip().split("\t")
    
    if (len(data) == 13):

        structureId, classification, experimentalTechnique, macromoleculeType, residueCount, resolution, structureMolecularWeight, crystallizationMethod, crystallizationTempK, densityMatthews, densityPercentSol, phValue, publicationYear = data
        if phValue!= '' and publicationYear!='' :
            o.write(publicationYear + '\t' + phValue + '\n')
            print publicationYear + '\t' + phValue 
        
i.close()
o.close()
