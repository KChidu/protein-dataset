input = open("mapper_output.txt","r")  # open file, read-only
output = open("sort_output.txt", "w") # open file, write
lines = input.readlines()  #reading lines
lines.sort()  #sorting data

for line in lines:
	output.write(line) #writing sorted data to output file
input.close()  #closing input file
output.close()  #closing output file