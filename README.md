# Protein Dataset

## Team name - Protien Sequences
## Project Group 2E
Shirisha Rapole(s530484@nwmissouri.edu)  
Uma Yandapalli(s530861@nwmissouri.edu)  
Keerthi Chiduruppa(s530711@nwissouri.edu)  
Bhavishya Arelli(s528734@nwmissouri.edu)  

## Project Pair 2-9
Uma Yandapalli, Keerthi Chiduruppa

## Project Pair 2-10
Shirisha Rapole, Bhavishya Arelli

## Dataset link:    
https://www.kaggle.com/shahir/protein-data-set/data   
 
## Introduction: 
The project mainly focuses on atomic coordinates and other information describing proteins and other important biological macromolecules. The structured data use methods such as X-ray crystallography, NMR spectroscopy, and cryo-electron microscopy to determine the location of each atom relative to each other in the molecule. 
The constantly-growing PDB is a reflection of the research that is happening in laboratories across the world. This can make it both exciting and challenging to use the database in research and education. 
## Dataset:

Structural Protein Dataset Number of records: 141402

Dataset size: 27MB 

File type: Excel

Format: Structured


## Link for the dataset Link for dataset (Structural Protein Dataset)- 

https://www.kaggle.com/shahir/protein-data-set/data

## Big data Qualifications 

VOLUME: The size of our data set is 27MB. 

VARIETY: Structured dataset is used in this project. 

VELOCITY: As the data used is data at rest the velocity is zero.

VERACITY: Data is taken from reliable sources and is clean. 

VALUE:To know the ph values and residue count based on the classification and experimental techniques used so that we can they can study about new diseases and find out the drugs to cure them.  

## Setup Instructions:
-You need to have Python 2.6.6 installed on your systems.  

-Clone the repository to know the bigdata solutions for the bigdata questions you are interested from the dataset.

## Big data Questions: 
-For each of the classfication what is the maximum and minimum residue count? - Shirisha Reddy Rapole

-For each year what is the average ph values and the highest ph value? - Bhavishya Arelli

-For each molecularType used what is the average structureMolecularWeight? - Uma Yandapalli

-For each crystallization method, what is the maximum densityPercent? - Keerthi Chiduruppa  

## Execution process:  
-After cloning the repository, open the GitBash terminal to execute the mapper and reducer respectively.  

-For running the mapper file use the command "python mapper.py"  

-After running the mapper file run the reducer file using the command "python reducer.py"  
(Run any intermediate file if required for sorting) 

Note: For the reference purpose the screenshots are embedded under each of big data solutions.  

## Big data Solutions:
-For each of the classfication what is the maximum and minimum residue count?  
	**Mapper input:**  100D, "DNA-RNA HYBRID", "X-RAY DIFFRACTION", "DNA/RNA Hybrid", 20, 1.9, 6360.3, "VAPOR DIFFUSION", HANGING DROP 1.78 30.89 pH 7.00, "VAPOR DIFFUSION", "HANGING DROP", 7.0, 1994  
	**Mapper output / Reducer input:**  Key: classification, Value: residue count  
	**Reducer output:** Key: classification, Value: residue count, Max:24, Min: 20  
	**Language:** Python   
	**Kind of chart:** Bar chart  
	![1.JPG](https://bitbucket.org/repo/BgdzbMq/images/3267209457-1.JPG)  
	**Your mapper output would be similar to the below screenshot:**  
	![MinMaxMapper.JPG](https://bitbucket.org/repo/Krz9zkM/images/1044050488-MinMaxMapper.JPG)
	**Your reducer output would be similar to the below screenshot:**  
	![MinMaxReducer.JPG](https://bitbucket.org/repo/Krz9zkM/images/2404934352-MinMaxReducer.JPG)
	
	
	
-For each year what is the average ph values and the highest ph value in the period of 1993-1999?      
 	**Mapper input:** 100D, "DNA-RNA HYBRID", "X-RAY DIFFRACTION", "DNA/RNA Hybrid", 20, 1.9, 6360.3, "VAPOR DIFFUSION", HANGING DROP 1.78 30.89 pH 7.00, "VAPOR DIFFUSION", "HANGING DROP", 7.0, 1994  
	**Mapper output / Reducer input:** Key: year, Value: PH Value  
	**Reducer output:** Key: year, Value: PH Value, highestPhValueYear : 1994, averagePhValue: 2.21  
	**Language:** Python  
	**Kind of chart:** Bar Chart  
	![2.JPG](https://bitbucket.org/repo/BgdzbMq/images/1684773865-2.JPG)  
	**Mapper screenshot:**  
	![HighPhmapper.JPG](https://bitbucket.org/repo/Krz9zkM/images/3572366376-HighPhmapper.JPG)  
	**Reducer screenshot:**  
	![HighPhreducer.JPG](https://bitbucket.org/repo/Krz9zkM/images/3231815072-HighPhreducer.JPG)  
	
-For each molecularType used what is the average structureMolecularWeight?    
	**Mapper input:** 100D, "DNA-RNA HYBRID", "X-RAY DIFFRACTION", "DNA/RNA Hybrid", 20, 1.9, 6360.3, "VAPOR DIFFUSION", HANGING DROP 1.78 30.89 pH 7.00, "VAPOR DIFFUSION", "HANGING DROP", 7.0, 1994  
	**Mapper output / Reducer input:** Key: molecularType, Value: structureMolecularWeight  
	**Reducer output:** Key: molecularType, Value: structureMolecularWeight , molecularType : DNA, averageMolecularWeight: 5701.73  
	**Language:** Python  
	**Kind of chart:** Bar Chart  
	![3.JPG](https://bitbucket.org/repo/BgdzbMq/images/3230549585-3.JPG)  
	**Mapper Screenshot:**  
	![molecularMapper.JPG](https://bitbucket.org/repo/Krz9zkM/images/2549866857-molecularMapper.JPG)  
	**Reducer Screenshot:**  
	![moleculartypereducer.JPG](https://bitbucket.org/repo/Krz9zkM/images/3873346827-moleculartypereducer.JPG)  
	
-For each crystallization method, what is the maximum densityPercent?      
	**Mapper input:** 100D, "DNA-RNA HYBRID", "X-RAY DIFFRACTION", "DNA/RNA Hybrid", 20, 1.9, 6360.3, "VAPOR DIFFUSION", HANGING DROP 1.78 30.89 pH 7.00, "VAPOR DIFFUSION", "HANGING DROP", 7.0, 1994  
	**Mapper output / Reducer input:** Key: crystallizationMethod, Value: densityPercent  
	**Reducer output:** Key: crystallizationMethod, Value: densityPercent , crystallaizationMethod: VAPOUR DIFFUSION  , maximumDensityPercent: 2.76  
	**Language:** Python  
	**Kind of chart:** Pie Chart  
	![4.JPG](https://bitbucket.org/repo/BgdzbMq/images/1923668386-4.JPG)  
	**Mapper screenshot:**  
	![crystallizationMapper.JPG](https://bitbucket.org/repo/Krz9zkM/images/4088314373-crystallizationMapper.JPG)  
	**Reducer screenshot:**  
	![crystallizationReducer.JPG](https://bitbucket.org/repo/Krz9zkM/images/2757381198-crystallizationReducer.JPG)  
	

