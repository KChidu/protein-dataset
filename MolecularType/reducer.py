input = open("sort_output.txt","r")   #opens the file-read only
output = open("reducer_output.txt", "w")  #opens the file-write only


type_weight = {}  #created an empty array


for line in input:
    
     line = line.strip()
     macromoleculeType, structureMolecularWeight  = line.split('\t')  #splitting the data file with tab delimited

    
     if macromoleculeType in type_weight:
        type_weight[macromoleculeType].append(float(structureMolecularWeight))
     else:
        type_weight[macromoleculeType] = []
        type_weight[macromoleculeType].append(float(structureMolecularWeight))

#calculating average using the key values

for macromoleculeType in type_weight.keys():
    ave_weight = sum(type_weight[macromoleculeType])*1.0 / len(type_weight[macromoleculeType])
    output.write('%s\t%s\n'% (macromoleculeType, ave_weight))
    print '%s\t%s\n'% (macromoleculeType, ave_weight)  #printing to the console

input.close() #closing the data file
output.close() #closing the mapper output file