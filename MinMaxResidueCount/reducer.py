s = open("sorted.txt","r")   
r = open("reducedValues.txt", "w")   

thisKey = ""
thisValue = 0.0
num_list = []
minValue = 0
maxValue = 0

# for loop is used to iterate the lines in the file and store it into two variables named classification and residueCount
for line in s:
    data = line.strip().split('\t')
    classification, residueCount = data
    if classification != thisKey: # for each key value we calculate the minimum and maximum values 
        if thisKey and thisKey!="": 
             r.write(thisKey + '\t' + str(maxValue) +'\t'+ str(minValue)+ '\n')
             print classification+'\t'+'Maximum: '+str(maxValue)+'\t'+'Minumum: ' +str(minValue)
        thisKey = classification # initializing values to default again
        thisValue = 0.0 
        maxValue=thisValue #intializing the maxValue with thisValue as default value as to find out the maximum residue count
        minValue=residueCount #intializing the minValue with residueCount as default value as to find out the minimum residue count
    if (residueCount>maxValue): #comparing each value of residueCount and if it is greater than the present value assign it to maxValue
        maxValue=residueCount
    if (residueCount<minValue): #comparing each value of residueCount and if it is less than the present value assign it to minValue
        minValue=residueCount

print '\n'
r.write(thisKey + '\t' + str(maxValue)+ '\t' + str(minValue) +'\n') 
print classification+'\t'+'Maximum: '+str(maxValue)+'\t'+'Minumum: ' +str(minValue)

s.close()
r.close()


