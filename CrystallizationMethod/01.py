o = open("mapperdata.txt", "r") # open file, read-only
s = open("mapoutput.txt", "w")# open file, write

for line in o:
    data = line.strip().split('\t')
    if len(data) != 2:  # if bad input line
       continue             # ignore it
    else:
        crystallizationMethod, densityPercentSol = data  # read into variables
        s.write(crystallizationMethod + '\t' + densityPercentSol + '\n')
o.close()
s.close()