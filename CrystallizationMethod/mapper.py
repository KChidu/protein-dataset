i = open("data.txt", "r") # open file, read-only

o = open("mapperdata.txt", "w") # open file, write

 

for line in i:

    data = line.strip().split(",")

    if (len(data) == 14):

        structureId, classification, experimentalTechnique, macromoleculeType, residueCount, resolution, structureMolecularWeight, crystallizationMethod, crystallizationTempK, densityMatthews, densityPercentSol, pdbxDetails, phValue, publicationYeartime = data
        o.write(crystallizationMethod + '\t' + densityPercentSol + '\n')

i.close()
o.close()